package controler;
import java.sql.*;
import java.sql.DriverManager;

//Connection
public class Connection {

    private java.sql.Connection connection;
    private Statement  stm;
    private ResultSet result;

    // static variable single_instance of type Singleton
    private static Connection single_instance = null;

    // private constructor restricted to this class itself
    public Connection(){
        connectDatabase("localhost", "5432", "postgres", "postgres", "monopoly9256");
    }

    // static method to create instance of Singleton class
    public static Connection getInstance(){
        if (single_instance == null)
            single_instance = new Connection();

        return single_instance;
    }

    //Function connect data base
    public void connectDatabase(String host, String port, String database, String user, String password) {
        String url = "";
        try {
            // We register the PostgreSQL driver
            // Registramos el driver de PostgresSQL
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                System.out.println("Error al registrar el driver de PostgreSQL: " + ex);
            }

            connection = null;
            url = "jdbc:postgresql://" + host + ":" + port + "/" + database;
            // Database connect
            // Conectamos con la base de datos

            connection = DriverManager.getConnection(
                    url,
                    user, password);
            boolean valid = connection.isValid(50000);
            System.out.println(valid ? "TEST OK" : "TEST FAIL");
        } catch (java.sql.SQLException sqle) {
            System.out.println("Error al conectar con la base de datos de PostgreSQL (" + url + "): " + sqle);
        }
    }


    //Receive the sentences sql to execute
    //Receive the sentences sql to execute
    public boolean executeSQL (String sql){
        boolean boolResult;

        //System.out.println(sql);
        try {
            boolResult = true;
            stm = connection.createStatement();//conection with data base
            if(searchSqlWord("UPDATE",sql) || searchSqlWord("INSERT",sql)){
                //System.out.println("Entro");
                stm.execute(sql);
            }else{
                //System.out.println("Etro Query");
                result = stm.executeQuery(sql);//execute sql
            }

        } catch (SQLException e) {
            boolResult = false;
            e.printStackTrace();
        }

        //System.out.println(boolResult);
        return boolResult;
    }

    //GET result
    public ResultSet getResult() {
        return result;
    }

    public boolean searchSqlWord(String word, String sql){
        boolean result = false;
        String[] words = sql.split("\\W+");
        for (String wordSearch : words) {
            if (wordSearch.contains(word)) {
                result = true;
            }
        }
        return result;
    }
}
