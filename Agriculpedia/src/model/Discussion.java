package model;

import org.jetbrains.annotations.Contract;

import java.util.Date;

public class Discussion {
    private String owner;
    private String creationdate;
    private int numcommentary;
    private String discussiontitle;

    public Discussion(){

    }

    @Contract(pure = true)
    public Discussion(String owner, String creationdate, int numcometary, String discussiontitle) {
        this.owner = owner;
        this.creationdate = creationdate;
        this.numcommentary = numcometary;
        this.discussiontitle = discussiontitle;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(String creationdate) {
        this.creationdate = creationdate;
    }

    public int getNumcommentary() {
        return numcommentary;
    }

    public void setNumcommentary(int numcommentary) {
        this.numcommentary = numcommentary;
    }

    public String getDiscussiontitle() {
        return discussiontitle;
    }

    public void setDiscussiontitle(String discussiontitle) {
        this.discussiontitle = discussiontitle;
    }

    @Override
    public String toString() {
        return "Discussion{" +
                "owner='" + owner + '\'' +
                ", creationdate='" + creationdate + '\'' +
                ", numcommentary=" + numcommentary +
                ", discussiontitle='" + discussiontitle + '\'' +
                '}';
    }
}
