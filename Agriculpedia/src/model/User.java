package model;

import org.jetbrains.annotations.Contract;


public class User {
    private String username;
    private String password;
    private String photourl;
    private String email;
    private String placeorigin;
    private String typecrop;


    public User(){

    }

    @Contract(pure = true)
    public User(String username, String password, String photourl, String email, String placeorigin, String typecrop) {
        this.username = username;
        this.password = password;
        this.photourl = photourl;
        this.email = email;
        this.placeorigin = placeorigin;
        this.typecrop = typecrop;
    }

    //SETTER AND GETTER

    public String getUsername() {
        return username;
    }

    public void setUserName(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhotourl() {
        return photourl;
    }

    public void setPhotourl(String photourl) {
        this.photourl = photourl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPlaceorigin() {
        return placeorigin;
    }

    public void setPlaceorigin(String placeorigin) {
        this.placeorigin = placeorigin;
    }

    public String getTypecrop() {
        return typecrop;
    }

    public void setTypecrop(String typecrop) {
        this.typecrop = typecrop;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + username + '\'' +
                ", password='" + password + '\'' +
                ", photourl=" + photourl +
                ", email='" + email + '\'' +
                ", placeorigin='" + placeorigin + '\'' +
                ", typecrop='" + typecrop + '\'' +
                '}';
    }
}
