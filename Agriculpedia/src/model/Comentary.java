package model;

import org.jetbrains.annotations.Contract;

public class Comentary {
    private String owner;
    private int discussioncommentaryid;
    private int score;
    private String description;
    private String title;
    private String date;
    private String hour;

    //Constructor
    @Contract(pure = true)
    public Comentary() {
    }

    @Contract(pure = true)
    public Comentary(String owner, int discussioncommentaryid, int score, String description, String title, String date, String hour) {
        this.owner = owner;
        this.discussioncommentaryid = discussioncommentaryid;
        this.score = score;
        this.description = description;
        this.title = title;
        this.date = date;
        this.hour = hour;
    }


    //SETTER AND GETTER

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public int getDiscussioncommentaryid() {
        return discussioncommentaryid;
    }

    public void setDiscussioncommentaryid(int discussioncommentaryid) {
        this.discussioncommentaryid = discussioncommentaryid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    //METHODS
    public void addScore(){
        this.score += 1;
    }

    public void subScore(){
        if(score != 0){
            this.score -= 1;
        }else{
            System.out.println("Score == 0 you can not subtract");
        }
    }

    //TO STRING
    @Override
    public String toString() {
        return "Comentary{" +
                "owner='" + owner + '\'' +
                ", discussioncommentaryid=" + discussioncommentaryid +
                ", score=" + score +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", date='" + date + '\'' +
                ", hour='" + hour + '\'' +
                '}';
    }

}
