package model;

import controler.Connection;
import java.sql.SQLException;

public class SentencesSQL  {

    Connection objConection;

    public SentencesSQL(){
        objConection =  Connection.getInstance();
    }

    public boolean insertUser(User user) {
        //TODO preguntar si el usuario existe en la base de datos
        String sql = "INSERT INTO public.usuario(\n"+
                "username, password, photourl, email, placeorigin, typecrop)\n" +
                "VALUES ('"+user.getUsername()+"', '"+user.getPassword()+"', '"+user.getPhotourl()+"', " +
                "'"+user.getEmail()+"', '"+user.getPlaceorigin()+"', '"+user.getTypecrop()+"')";

        return objConection.executeSQL(sql);
    }

    public boolean insertCometary(Comentary comentary){
        String sql = "INSERT INTO public.commentary(\n" +
                "owner, discussioncommentaryid, score, description, title,\n" +
                "date, hour)\n" +
                "VALUES ('"+comentary.getOwner()+"', "+comentary.getDiscussioncommentaryid()+", "+comentary.getScore()+", " +
                "'"+comentary.getDescription()+"', '"+comentary.getTitle()+"'," +
                "'"+comentary.getDate()+"', '"+comentary.getHour()+"')";

        return objConection.executeSQL(sql);
    }

    public boolean insertDiscution(Discussion discussion){
        String sql = "INSERT INTO public.discussion(\n"+
                "owner, creationdate, numcommentary, discussiontitle)\n" +
                "VALUES ('"+discussion.getOwner()+"', '"+discussion.getCreationdate()+"', "+discussion.getNumcommentary()+", " +
                "'"+discussion.getDiscussiontitle()+"')";

        return objConection.executeSQL(sql);
    }


    public int selectComentaryScore(String username){
        int score = 0;

        String sql = " SELECT score \n" +
                "  FROM public.commentary WHERE owner = '" + username + "'";

        try {
            objConection.executeSQL(sql);
            objConection.getResult().next();
            score  = objConection.getResult().getInt(1);

        } catch (SQLException ex) {
            //System.out.println("Problema buscando en la Base de Datos");
            score = -1;
        }

        return score;
    }


    public boolean updateComentaryScore(String username, int flag){

        int newScore = selectComentaryScore(username);

        if (newScore == -1){
            return false;
        }

        if (flag == 0){
            newScore += 1;
        }else {
            newScore -= 1;
        }


        String sql = "UPDATE public.commentary\n" +
                "SET score = "+ newScore +" WHERE  owner = '" + username + "'";

        return objConection.executeSQL(sql);
    }


    public User selectUser(String username){
        User user = new User();

        String sql = " SELECT * from public.usuario WHERE username = '"+username+"'";

        try {
            objConection.executeSQL(sql);
            objConection.getResult().next();
            user.setUserName(objConection.getResult().getString(1));
            user.setPassword(objConection.getResult().getString(2));
            user.setPhotourl(objConection.getResult().getString(3));
            user.setEmail(objConection.getResult().getString(4));
            user.setPlaceorigin(objConection.getResult().getString(5));
            user.setTypecrop(objConection.getResult().getString(6));

        } catch (SQLException ex) {
            System.out.println("Problema Buscando en la Base de Datos");
        }

        return user;
    }

    public boolean delete(){
        String sql = "";

        return objConection.executeSQL(sql);
    }




}
