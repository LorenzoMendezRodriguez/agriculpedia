import model.SentencesSQL;
import org.junit.Test;

import static org.junit.Assert.*;

public class SentencesSQLTest {
    private SentencesSQL obSentencesSQL = new SentencesSQL();


    @Test
    public void selectComentaryScore() {
        /*
        Given a user named Lorenzo, with a commentary with a score of 0
        When the function selectComentaryScore with Melanie as the owner is called
        Then it should return the actual score of the commentary, which is 0
        */
        
        assertEquals(0, obSentencesSQL.selectComentaryScore("Melanie"));
    }

    @Test
    public void selectComentaryScoreNonExistingUser() {
        /*
        Given a non-existing user named Sebas
        When the function selectComentaryScore with Sebas as input
        Then it should return -1, proof that the backend validates this properly
        */

        assertEquals(-1, obSentencesSQL.selectComentaryScore("Sebas"));
    }

    @Test
    public void updateComentaryScoreWithExistentUser() {
        /*
        Given an existing user named Lorenzo with a comment stored in the database
        When the function updateComentaryScore with Lorenzo as the owner is called
        Then it should return the boolean value of true, implying that the sql sentence is being executed properly
        */

        //Banderas para la funcion de update
        int ADD = 0;
        int SUB = 1;
        assertEquals(true, obSentencesSQL.updateComentaryScore("Lorenzo",ADD));
    }

    @Test
    public void checkUpdateComentaryScoreWithExistentUserAdd() {
        /*
        Given an existing user named Lorenzo with a comment stored in the database
        When the function updateComentaryScore with Lorenzo as the owner is called and with the ADD flag
        Then it should the original value before the execution plus one
        */

        //Banderas para la funcion de update
        int ADD = 0;
        int SUB = 1;
        int expected = obSentencesSQL.selectComentaryScore("Lorenzo") + 1;
        obSentencesSQL.updateComentaryScore("Lorenzo",ADD);
        assertEquals(expected, obSentencesSQL.selectComentaryScore("Lorenzo") );
    }

    @Test
    public void checkUpdateComentaryScoreWithExistentUserSubstract() {
        /*
        Given an existing user named Lorenzo with a comment stored in the database
        When the function updateComentaryScore with Lorenzo as the owner is called and with the SUB flag
        Then it should the original value before the execution minus one
        */

        //Banderas para la funcion de update
        int ADD = 0;
        int SUB = 1;
        int expected = obSentencesSQL.selectComentaryScore("Lorenzo") -1;
        obSentencesSQL.updateComentaryScore("Lorenzo",SUB);
        assertEquals(expected, obSentencesSQL.selectComentaryScore("Lorenzo") );
    }

    @Test
    public void updateComentaryScoreWithNonExistentUser() {
        /*
        Given a non-existing user named Sebas
        When the function updateComentaryScore with Sebas
        Then it should false, proof that the backend validates this properly
        */

        //Banderas para la funcion de update
        int ADD = 0;
        int SUB = 1;
        assertEquals(false, obSentencesSQL.updateComentaryScore("Sebas",ADD));
    }



}